// Accordions{
$('.accordion__header').click(function(){
	var $item = $(this).closest('.accordion__item');
	$('.accordion__item').not($item).removeClass('accordion__item--is-expanded');

	$item.toggleClass('accordion__item--is-expanded');
});