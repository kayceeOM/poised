// Carousel
$('.carousel').flickity({
  setGallerySize: false,
  wrapAround: true,
  autoPlay: 3000,
  pauseAutoPlayOnHover: false,
  selectedAttraction: 0.007,
  friction: 0.15
});

var flkty = new Flickity( '.testimonial-carousel', {
  cellAlign: 'left',
  contain: true,
  wrapAround: true,
  prevNextButtons: false,
  autoPlay: 5000
});
