$(window).on('scroll', function () {
    if ($(window).scrollTop() >= 130){
        $('.nav').addClass('nav--fixed');
        var $subnav = $('.sub-nav');
        if($subnav.length){
        	$subnav.first().addClass('sub-nav--fixed');	
        	$('.header').addClass('header--has-fixed-subnav');
        }else{
        	$('.header').addClass('header--has-fixed-nav');
        }
    }else{
        $('.nav').removeClass('nav--fixed');
        $('.sub-nav').removeClass('sub-nav--fixed');
        $('.header').removeClass('header--has-fixed-nav');
        $('.header').removeClass('header--has-fixed-subnav');
    }
});

